﻿using CentroEducativo.Models;
using CentroEductivo.Data;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CentroEducativo.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private CentroEducativoDbContext db;
        IHostingEnvironment _env;

        public UsuariosController(CentroEducativoDbContext context, IHostingEnvironment environment)
        {
            db = context;
            _env = environment;



        }
        // GET: api/<UsuariosController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuarios>>> Get()
        {
            return  db.usuarios.ToList();
        }

        // GET api/<UsuariosController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuarios>> Get(int id)
        {
            return await db.usuarios.FindAsync(id);
        }

        // POST api/<UsuariosController>
        [HttpPost]
        public async Task<ActionResult<Usuarios>> Post(Usuarios usuario)
        {
            db.usuarios.Add(usuario);
            await db.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(nameof(Get), new { id = usuario.IdUsuario }, usuario);
        }

        // PUT api/<UsuariosController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Usuarios usuarios)
        {
            /* if (id != usuarios.IdUsuario)
             {
                 return BadRequest();
             }*/

            var us = db.usuarios.Where(x => x.IdUsuario == id).FirstOrDefault();
           // db.Entry(usuarios).State = EntityState.Modified;

            us.Nombre1 = usuarios.Nombre1;
            us.Nombre2 = usuarios.Nombre2;
            us.Apellido1 = usuarios.Apellido1;
            us.Apellido2 = usuarios.Apellido2;
            us.Identificacion = usuarios.Identificacion;
            us.Email = usuarios.Email;
            us.Celular = usuarios.Celular;
            us.Direccion = usuarios.Direccion;
            us.Ciudad = usuarios.Ciudad;

            try
            {
                await db.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (us == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new
            {
                response = "Se ha actualizado el libro",
                status = 1
            });
        }
        private bool UsuariosExists(int id) =>   db.usuarios.Any(e => e.IdUsuario == id);
        // DELETE api/<UsuariosController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

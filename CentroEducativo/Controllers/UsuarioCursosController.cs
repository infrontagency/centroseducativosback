﻿using CentroEducativo.Models;
using CentroEducativo.Models.Transactions;
using CentroEductivo.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CentroEducativo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioCursosController : ControllerBase
    {
        // GET: api/<UsuarioCursos>
        private CentroEducativoDbContext db;
        IHostingEnvironment _env;

        public UsuarioCursosController(CentroEducativoDbContext context, IHostingEnvironment environment)
        {
            db = context;
            _env = environment;



        }
        [HttpGet]
        public  IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpPost]
        public async Task<ActionResult<UsuarioCurso>> Post(UsuarioCurso usuarioCurso)
        {
            db.usuarioCursos.Add(usuarioCurso);
            await db.SaveChangesAsync();

            return NoContent();//CreatedAtAction(nameof(Get), new { id = usuarioCurso.IdUsuario }, usuarioCurso);
        }
        // GET api/<UsuarioCursos>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<GetUsuarioCursos>>> Get(int id)
        {


            var dsa = await  db.usuarios.ToListAsync();
            //List<Usuarios> cur =await  db.usuarios.Include(c => c.roles).ToListAsync();

             var UsuarioCursos = await (
                  from usuarioCurso in db.usuarioCursos
                  join usuario in db.usuarios on usuarioCurso.IdUsuario equals usuario.IdUsuario
                  join curos in db.cursos on usuarioCurso.IdCursos equals curos.IdCursos
                  where usuario.IdUsuario == id
                  select new GetUsuarioCursos() {
                     Nombre1 = usuario.Nombre1,
                      Nombre2 = usuario.Nombre2,
                      Apellido1 = usuario.Apellido2,
                      Identificacion = usuario.Identificacion,
                      Email = usuario.Email,
                      Celular = usuario.Celular,
                      Direccion = usuario.Direccion,
                      Ciudad = usuario.Ciudad,
                      NombreCurso = curos.NombreCurso,
                      IdCurso = curos.IdCursos,
                      IdUsuarioCursos = usuarioCurso.IdUsuarioCurso
                  }).ToListAsync();

            return UsuarioCursos;
        }

        // POST api/<UsuarioCursos>
        [HttpGet]
        [Route("CursosNuevos/{id}")]
        public async Task<ActionResult<IEnumerable<Cursos>>> CursosNuevos (int id)
        {

           List<UsuarioCurso> usuarioCursos =  db.usuarioCursos.Where(x => x.IdUsuario == id).ToList();
            List<Cursos> Cursos2 = db.cursos.ToList();
            foreach (var item in usuarioCursos)
            {
                Cursos cursos = db.cursos.Where(x=>x.IdCursos == item.IdCursos).FirstOrDefault();

                Cursos2.Remove(cursos);
            }
            
            return Cursos2;
        }

        // PUT api/<UsuarioCursos>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UsuarioCursos>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UsuarioCurso>> Delete(int id)
        {
            UsuarioCurso usuarioCurso = db.usuarioCursos.Where(x => x.IdUsuarioCurso == id).FirstOrDefault();

            db.usuarioCursos.Remove(usuarioCurso);

           await  db.SaveChangesAsync();

            return usuarioCurso;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentroEducativo.Models
{
    public class Usuarios
    {
        [Key]
        public int IdUsuario { get; set; }
        public string Identificacion { get; set; }
        public string Nombre1 { get; set; }
        public string Nombre2 { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
       //public List<Cursos> cursos { get; set; }
        /*Realaciones*/

        public int IdRol { get; set; }
        public Roles roles {get;set;}

        public ICollection<UsuarioCurso> GetUsuarioCursos { get; set; }

    }
}

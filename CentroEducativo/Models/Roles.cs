﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentroEducativo.Models
{
    public class Roles
    {
        [Key]
        public int IdRol{get;set;}
        public string  NombreRol {get;set;}


        public ICollection<Usuarios> GetUsuarios { get; set; }
    }
}

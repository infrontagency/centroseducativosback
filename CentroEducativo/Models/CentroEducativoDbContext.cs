﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentroEducativo.Models;
using Microsoft.EntityFrameworkCore;

namespace CentroEductivo.Data
{
    public class CentroEducativoDbContext : DbContext
    {
        public CentroEducativoDbContext(DbContextOptions<CentroEducativoDbContext> options)
            :base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Relation Users
            modelBuilder.Entity<Usuarios>().HasOne(x => x.roles).WithMany(c => c.GetUsuarios).HasForeignKey(x => x.IdRol).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<UsuarioCurso>().HasOne(x => x.usuarios).WithMany(c => c.GetUsuarioCursos).HasForeignKey(x => x.IdUsuario).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<UsuarioCurso>().HasOne(x => x.cursos).WithMany(c => c.GetUsuarioCursos).HasForeignKey(x => x.IdCursos).OnDelete(DeleteBehavior.NoAction);

            //roles

            //booksgenre
        }
        public DbSet<Usuarios> usuarios { get; set; }
        public DbSet<Cursos> cursos { get; set; }
        public DbSet<UsuarioCurso> usuarioCursos { get; set; }
        public DbSet<Roles> roles { get; set; }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentroEducativo.Models
{
    public class UsuarioCurso
    {
        [Key]
        public int IdUsuarioCurso { get; set; }

        //Relaciones
        public int IdUsuario { get; set; }
        public Usuarios usuarios { get; set; }
        public int IdCursos { get; set; }
        public Cursos cursos { get; set; }

    }
}

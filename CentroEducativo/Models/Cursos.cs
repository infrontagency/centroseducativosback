﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CentroEducativo.Models
{
    public class Cursos
    {
        [Key]
        public int IdCursos { get; set; }
        public string NombreCurso { get; set; }
        public int NumeroCreditos { get; set; }
        //public List<Usuarios> usuarios {get;set;}
        public ICollection<UsuarioCurso> GetUsuarioCursos { get; set; }
    }
}
